httplib2==0.9.2
pyshorteners==0.6.1
git+https://github.com/shackra/python-instagram
requests==2.9.1
simplejson==3.8.1
six==1.10.0
toml==0.9.1
