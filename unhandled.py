#!/usr/bin/python3

import asyncio
import logging
import traceback
import pprint
from io import StringIO
import smtplib
from os.path import basename, join, expanduser
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate

# Change this if yor SMTP server is somewhere else than localhost
SMTP_SERVER = "127.0.0.1"
# IMPORTANT: Change this to your e-mail address
TO = "CHANGE@THIS.ADDRESS"
SUBJECT = "Instagram-love bot  unexpectedly stopped working!"
BODY = """Instagram-love bot has encountered an un-handled exception and was closed to
avoid further malfunction. Attached is the log file, please forward it to the
original developer at PeoplePerHour."""

logf = join(expanduser("~"), ".config", "instalove", "logging.log")


def sendemail():
    """ Sends an email when the bot catch an un-handled exception
    """
    msg = MIMEMultipart(
        From="instagram-love@localhost",
        To=TO,
        Date=formatdate(localtime=True),
        Subject=SUBJECT)
    msg.attach(MIMEText(BODY))
    with open(logf, "rb") as file_:
        attachment = MIMEApplication(
            file_.read(),
            Content_Disposition='attachment; filename="%s"' % basename(logf),
            Name=basename(logf))
        msg.attach(attachment)
    with smtplib.SMTP(SMTP_SERVER) as smtp_:
        smtp_.sendmail("instagram-love@localhost", TO, msg.as_string())


def unhandledexception(exctype, excvalue, tracebackobj):
    """Hook for catching any un-handled exception.
    :param exctype: exception type
    :param excvalue: exception value
    :param tracebackobj: traceback object
    """
    loop = asyncio.get_event_loop()
    tracebackinfo = StringIO()
    # Print the traceback
    traceback.print_tb(tracebackobj, None, tracebackinfo)
    # Print all the variables when the exception happened
    print("\nAll defined variables when the exception happened:\n",
          file=tracebackinfo)
    pprint.pprint(tracebackobj.tb_frame.f_globals, tracebackinfo)
    tracebackinfo.seek(0)

    # Send the gathered information into the logs
    logging.critical(tracebackinfo.read())
    loop.close()
    logging.info("Loop closed")
    logging.info("Exiting...")
    # send an email
    try:
        sendemail()
    except ConnectionRefusedError:
        logging.error(
            "Connection with localhost refused, cannot notify user by email")
    # close the loop
    exit(1)
