#!/usr/bin/python3
import argparse
import asyncio
import logging
import sys
from datetime import datetime
from os import path, makedirs
from random import randint

import toml
from instagram.bind import InstagramAPIError
from instagram.client import InstagramAPI
from pyshorteners import Shortener

import unhandled


sys.excepthook = unhandled.unhandledexception

LOCALCONF = path.join(path.dirname(__file__), "conf.toml")
HOMECONF = path.join(path.expanduser("~"), ".config", "instalove", "conf.toml")

if not path.isdir(path.dirname(HOMECONF)):
    makedirs(path.dirname(HOMECONF))

arg = argparse.ArgumentParser()
arg.add_argument("--debug", help="Show debugging information",
                 action="store_true", default=False)
arg.add_argument("--re-auth", help="Authenticate the bot again when needed",
                 action="store_true", default=False)


def auth(force_local=False, again=False):
    """ Take the user thought the authentication process

    :param force_local: Load the configuration relative to this script instead
    :type force_local: bool
    """
    if force_local:
        filepath = LOCALCONF
    else:
        filepath = HOMECONF

    try:
        with open(filepath) as fil:
            appdata = toml.load(fil)
    except FileNotFoundError:
        appdata = {}

    if "client_id" not in appdata or again:
        logging.error("Instagram app client ID not found!")
        clientid = str(input("Client ID: ").strip())
        appdata["client_id"] = clientid

    if "client_secret" not in appdata or again:
        logging.error("Instagram app client secret not found!")
        clientsecret = str(input("Client secret: ").strip())
        appdata["client_secret"] = clientsecret

    if "access_token" not in appdata or again:
        logging.error("The bot is not allowed by your Instagram "
                      "account to do things in your behalf!")
        client = InstagramAPI(client_id=appdata["client_id"],
                              client_secret=appdata["client_secret"],
                              redirect_uri="http://localhost")
        shortener = Shortener('Tinyurl')
        authurl = client.get_authorize_login_url(scope=["basic", "relationships",
                                                        "likes", "public_content"])
        shorturl = shortener.short(authurl)
        logging.info("Please, visit %s on your web browser", shorturl)
        logging.info("Please, copy the code in http://localhost/?code=<code> "
                     "after you are redirected. Insert it below here")
        code = str(input("Instagram code: ").strip())
        answer = client.exchange_code_for_access_token(code)
        logging.debug(answer)
        access_token = answer[0]
        appdata["access_token"] = access_token

    with open(filepath, "w") as fil:
        toml.dump(appdata, fil)

    logging.info("done.")


class thinkagain:

    """ Class decorator, makes calling :class:`InstagramLove.think` after a
    decorated method is called.
    """
    loop = asyncio.get_event_loop()
    instance_ = None

    def __init__(self, f):
        self.fun = f

    def __get__(self, instance, owner):
        self.instance_ = instance
        return self.__call__

    def __call__(self, *args, **kwords):
        timestamp = datetime.utcnow()
        name = "{}_called".format(self.fun.__name__)
        setattr(self.instance_, name, timestamp)

        handler = getattr(self.instance_, "think_handler", None)
        if handler:
            # cancel the last call to `InstagramLove.think` method
            handler.cancel()

        new_handler = self.loop.call_later(10 * 60, self.instance_.think)
        setattr(self.instance_, "think_handler", new_handler)

        return self.fun(self.instance_, *args, **kwords)


class InstagramLove:

    """Holds the list of users and app data/configuration.
    Schedule tasks and so on
    """
    ONEHOUR = 1 * 60 * 60
    ONEMINUTE = 1 * 60
    loop = asyncio.get_event_loop()

    def __init__(self, force_local=False):
        self.force_local = force_local
        try:
            self.appdata = self.loadconf()
            self.client = InstagramAPI(access_token=self.appdata["access_token"],
                                       client_secret=self.appdata["client_secret"])
            logging.info("Instagram client created successfully")
        except FileNotFoundError:
            self.appdata = {}

    def _howlong(self, funcname):
        """ Return how many seconds has passed since last call of `func`
        """
        name = "{}_called".format(funcname)
        try:
            last_time = getattr(self, name)
        except AttributeError:
            logging.warning("attribute %s didn't exist", name)
            return 86400
        else:
            now = datetime.utcnow()
            time_passed = now - last_time
            logging.debug(
                "Last timestamp: %s - Current timestamp: %s", last_time, now)
            logging.debug("Seconds passed: %s", time_passed.seconds)
            return time_passed.seconds

    def _cancelhandler(self, funcname, newhandler):
        """ Cancel an old handler and schedule a new one
        """
        name = "{}_hanlder".format(funcname)
        try:
            handler = getattr(self, name)
            handler.cancel()
        except AttributeError:
            logging.warning("No handler %s defined", name)

        setattr(self, name, newhandler)

    @thinkagain
    def think(self):
        """ Decides what to do and which method to schedule

        This method is a **very** primitive form of AI.
        """
        if "following" not in self.appdata:
            logging.info("Followed users never fetch. Fetch scheduled")
            self.loop.call_soon(self.fetchfollowing)
        else:
            if self._howlong("fetchfollowing") >= self.ONEHOUR:
                self.loop.call_soon(self.fetchfollowing)

        if self._howlong("saveconf") >= self.ONEMINUTE * 10:
            self.loop.call_soon(self.saveconf)

        if self._howlong("love") >= self.ONEHOUR:
            # FIFO = first in, first out
            self.loop.call_soon(self.preparelove)
            self.loop.call_soon(self.love)

    def loadconf(self):
        """ Returns a dictionary

        :param force_local: Load conf file relative to this script
        :returns: a Python dictionary
        :rtype: dict
        """
        if not self.force_local:
            filepath = HOMECONF
        else:
            filepath = LOCALCONF

        with open(filepath) as fil:
            return toml.load(fil)

    @thinkagain
    def saveconf(self):
        """ Save a dictionary as TOML

        Schedule itself to be called in 10 minutes
        """
        logging.info("Saving configuration and app data...")
        if not self.force_local:
            with open(HOMECONF, "w") as homef:
                toml.dump(self.appdata, homef)
        else:
            with open(LOCALCONF, "w") as localf:
                toml.dump(self.appdata, localf)
        logging.info("Configuration and app data saved successfully!")

    def sendlike(self, media_id):
        """ Likes the media of a user

        :param media_id: Instagram ID of the media
        :type media_id: int
        """
        if int(self.client.x_ratelimit_remaining) > 0:
            self.client.like_media(media_id)
            logging.debug("Liked media id: %s", media_id)
        else:
            logging.warning("Cannot like media %s at this moment:"
                            " API rate limit exceeded", media_id)
            wait = randint(self.ONEMINUTE * 4, self.ONEHOUR / 2) + self.ONEHOUR
            logging.info("Rescheduling it for later in %s seconds", wait)
            self.loop.call_later(wait, self.sendlike, media_id)

    @thinkagain
    def fetchfollowing(self):
        """ Get the users this user follows

        .. warnings also:: This method assumes we haven't exceeded the API rate
        """
        logging.info("Fetching followed users...")
        users = []

        try:

            following, next_ = self.client.user_follows(user_id="self")
            if following:
                users.extend(following)

            while next_ and int(self.client.x_ratelimit_remaining) > 0:
                following_more, next_ = self.client.user_follows(
                    user_id="self",
                    with_next_url=next_)
                users.extend(following_more)

            if "following" not in self.appdata:
                self.appdata["following"] = {}

            for user in users:
                if user.username not in self.appdata["following"]:
                    self.appdata["following"][
                        user.username] = {"user_id": user.id}

            current_list = [x for x in self.appdata["following"].keys()]
            new_list = [x.username for x in users]
            # remove the repeated elements from both lists, this will show what
            # elements are unique on the old list (current_list).
            not_following = list(set(new_list) - set(current_list))
            if len(not_following):
                logging.info("Number of users not followed"
                             " anymore to be deleted %s", len(not_following))
                for user in not_following:
                    del self.appdata["following"][user]

            logging.info("Fetched followed users successfully!")
        except InstagramAPIError as err:
            logging.error("Error while fetching for users")
            logging.error("Code %s (%s)", err.status_code, err.error_type)
            logging.error(err.error_message)

    def fetchrecentmedia(self, user_id, max_id=None):
        """ Get new media posted by an user

        .. warnings also:: This method assumes we haven't exceeded the API rate

        :param user_id: User ID on Instagram
        :type  user_id: int
        :param max_id: Fetch media since media ID
        :type max_id: int
        """
        recent_media = []

        try:
            if max_id:
                media, next_ = self.client.user_recent_media(
                    user_id, max_id=max_id)
            else:
                media, next_ = self.client.user_recent_media(user_id)

            if media:
                recent_media.extend(media)

            while next_ and int(self.client.x_ratelimit_remaining) > 0:
                more_media, next_ = self.client.user_recent_media(
                    user_id, with_next_url=next_)
                recent_media.extend(more_media)
        except InstagramAPIError as err:
            logging.error("Error while fetching recent media")
            logging.error("Code %s (%s)", err.status_code, err.error_type)
            logging.error(err.error_message)

        logging.debug("X-Ratelimit-remaining: %s",
                      self.client.x_ratelimit_remaining)
        return recent_media

    @thinkagain
    def preparelove(self):
        """ Make a list of users to fetch their recent media
        """
        if "tolove" not in self.appdata:
            self.appdata["tolove"] = [
                x for x in self.appdata["following"].keys()]
        else:
            if self.appdata["tolove"]:
                logging.warning(
                    "Using the old stack until finished sending love")
            else:
                self.appdata["tolove"] = [
                    x for x in self.appdata["following"].keys()]

    @thinkagain
    def love(self):
        """ Schedule recent media of users to be liked

        .. warnings also:: This method assumes we haven't exceeded the API rate
        """
        while int(self.client.x_ratelimit_remaining) > 0 and self.appdata["tolove"]:
            instuser = self.appdata["tolove"][-1]
            instuserdata = self.appdata["following"][instuser]
            recentmedia = self.fetchrecentmedia(**instuserdata)
            max_id_pool = []
            # Let's schedule tasks
            for media in recentmedia:
                # calculate a random interval
                # From 4 minutes up to 30 minutes
                interval = randint(self.ONEMINUTE * 4, self.ONEHOUR / 2)
                self.loop.call_later(interval, self.sendlike, media.id)
                max_id_pool.append(media.id)

            self.appdata["tolove"].pop()
            logging.info("Users to like their media left: %s", len(
                self.appdata["tolove"]))
            if recentmedia:
                max_id_pool.sort()
                max_id = max_id_pool[-1]
                logging.debug("Maximums media ID for %s: %s", max_id, instuser)
                # Update the maximums media ID for this user
                self.appdata["following"][instuser]["max_id"] = max_id


if __name__ == "__main__":
    # When the script is executed
    parse = arg.parse_args()
    if parse.debug:
        level = logging.DEBUG
    else:
        level = logging.INFO

    HOMELOGGING = path.join(path.dirname(HOMECONF), "logging.log")

    # set up logging to file - see previous section for more details
    logging.basicConfig(level=level,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%m-%d %H:%M',
                        filename=HOMELOGGING,
                        filemode='w')
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(level)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    logging.info("Everything is saved/loaded at %s", path.dirname(HOMECONF))
    auth(False, parse.re_auth)
    instlov = InstagramLove(False)
    loop = asyncio.get_event_loop()
    loop.set_debug(parse.debug)
    try:
        logging.info("Loop started")
        loop.call_soon(instlov.think)
        loop.run_forever()
    except KeyboardInterrupt:
        logging.info("Loop exited")
        loop.close()
